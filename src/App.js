import React, { useState } from "react";
import htmlToImage from "html-to-image";
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from "reactstrap";

import logo from "./logo.svg";
import "./App.css";

function App() {
  const [link, setLink] = useState({ href: "" });
  const [modal, setModal] = useState(false);

  const toggle = () => setModal(!modal);

  const generateImage = () => {
    htmlToImage
      .toJpeg(document.getElementById("my-node"), { quality: 0.95 })
      .then(function (dataUrl) {
        var link = document.createElement("a");
        link.download = "my-image-name.jpeg";
        link.href = dataUrl;
        setLink(link);
        console.log(link);
        toggle();
      });
  };
  const downloadImage = () => {
    link.click();
  };

  return (
    <div className="App">
      <div id="my-node">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <p>
            Edit <code>src/App.js</code> and save to reload.
          </p>
          <a
            className="App-link"
            href="https://reactjs.org"
            target="_blank"
            rel="noopener noreferrer"
          >
            Learn React
          </a>
          this is going to be the div that is going to be downloaded
          <Button onClick={generateImage}>Generate Image</Button>
          <Modal isOpen={modal} toggle={toggle}>
            <ModalHeader toggle={toggle}>Image Download</ModalHeader>
            <ModalBody>
              <div>
                <img src={link.href} width={"100%"}></img>
              </div>
            </ModalBody>
            <ModalFooter>
              <Button color="primary" onClick={downloadImage}>
                Download
              </Button>{" "}
              <Button color="secondary" onClick={toggle}>
                Cancel
              </Button>
            </ModalFooter>
          </Modal>
        </header>
      </div>
    </div>
  );
}

export default App;
